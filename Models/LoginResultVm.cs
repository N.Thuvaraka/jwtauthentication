

namespace JWTAuthentication.Models;

public class LoginResultVm
{
    public string? token { get; set; }


    public DateTime? expiration { get; set; }

}


