using System.ComponentModel.DataAnnotations;
namespace JWTAuthentication
{
    public class ForgotPassword
    {
        [Required]
        [EmailAddress]
        public string? Email { get; set; }
        public string? CurrentPassword { get; set; }
        public string? NewPassword { get; set; }

        public int Id { get; set; }
    }
}