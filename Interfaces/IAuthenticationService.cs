using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JWTAuthentication.Models;

namespace JWTAuthentication.Interfaces
{
    public interface IAuthenticationService
    {
        Task<LoginResultVm> Login(LoginModel model);
        Task<RegisterResultVm> Register(RegisterModel model);
        Task<ForgotPasswordResultVm> ForgotPassword(ForgotPassword model);
    }
}