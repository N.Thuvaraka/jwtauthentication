using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using JWTAuthentication.Interfaces;
using JWTAuthentication.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace JWTAuthentication.Services;

public class AuthenticationService : IAuthenticationService

//login starts
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly IConfiguration _configuration;


    public AuthenticationService(UserManager<IdentityUser> userManager,
    IConfiguration configuration)
    {
        _userManager = userManager;
        _configuration = configuration;
    }


    public async Task<LoginResultVm> Login(LoginModel model)
    {
        var user = await _userManager.FindByEmailAsync(model.Email);
        var password = await _userManager.CheckPasswordAsync(user, model.Password);

        if (user != null && password)
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),

            };

            foreach (var userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }
            var token = GetToken(authClaims);

            return new LoginResultVm
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
            };
        }
        else
        {
            throw new Exception(message: "user does not exist");
        }
    }

    // login ends

    //register starts 

    public async Task<RegisterResultVm> Register(RegisterModel model)
    {
        IdentityUser user = new()
        {
            Email = model.Email,
            SecurityStamp = Guid.NewGuid().ToString(),
            UserName = model.UserName
        };

        var result = await _userManager.CreateAsync(user, model.Password);
        if (!result.Succeeded)
        {

            throw new Exception(result?.Errors.FirstOrDefault().Description);
        }
        // return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = $"{result?.Errors.FirstOrDefault().Description}" });

        return new RegisterResultVm { Status = "Success", Message = "User created successfully!" };
    }

    //register ends

    //forgot password starts here
    public async Task<ForgotPasswordResultVm> ForgotPassword(ForgotPassword model)
    {
        var userExists = await _userManager.FindByEmailAsync(model.Email);
        if (userExists == null)
            throw new Exception("user does not exist");

        var userAfterpasswordChange = await _userManager.ChangePasswordAsync(userExists, model.CurrentPassword, model.NewPassword);

        if (userAfterpasswordChange?.Errors.Any() == true)
        {
            return new ForgotPasswordResultVm
            {
                Status = "Error",
                Message = "Password incorrect"
            };
        }

        return new ForgotPasswordResultVm
        {
            Status = "Success",
            Message = $"Password has been changed. Your new password is {model.NewPassword}"

        };
    }
    //forgot password ends here

    private JwtSecurityToken GetToken(List<Claim> authClaims)
    {
        var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

        var token = new JwtSecurityToken(
            issuer: _configuration["JWT:ValidIssuer"],
            audience: _configuration["JWT:ValidAudience"],
            expires: DateTime.Now.AddHours(3),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        );

        return token;
    }

}

