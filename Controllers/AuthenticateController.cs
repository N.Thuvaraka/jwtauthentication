using JWTAuthentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Models;
using JWTAuthentication.Models;
using JWTAuthentication.Interfaces;

public class ArgumentNullException : ArgumentException { }
namespace JWTAuthenticationController
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AuthenticateController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IAuthenticationService _authenticationService;



        public AuthenticateController(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration,
            ApplicationDbContext applicationDbContext,
            IAuthenticationService authenticationService
        )

        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _applicationDbContext = applicationDbContext;
            _authenticationService = authenticationService;

        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResultVm>> Login([FromBody] LoginModel model)
        {
            return await _authenticationService.Login(model);
        }


        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<RegisterResultVm>> Register([FromBody] RegisterModel model)
        {
            var result = await _authenticationService.Register(model);
            return result;
        }

        [HttpPost("ForgotPassword")]
        [AllowAnonymous]
        public async Task<ActionResult<ForgotPasswordResultVm>> ForgotPassword([FromBody] ForgotPassword model)
        {
            var result = await _authenticationService.ForgotPassword(model);
            return result;

        }

        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            );

            return token;
        }
    }
}